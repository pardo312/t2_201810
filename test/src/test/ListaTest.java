package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Date;

import org.junit.Test;

import model.data_structures.Lista;

import model.vo.Taxi;

public class ListaTest 
{
	Lista<Taxi> lista = null;
	Taxi taxi1 = new Taxi("Company1", "Taxi_id1");
	Taxi taxi2 = new Taxi("Company2", "Taxi_id2");
	Taxi taxi3 = new Taxi("Company3", "Taxi_id3");
	
	public void setupEscenario1( )
    {
		lista = new Lista<Taxi>();
		
		lista.add(taxi1);
		lista.add(taxi2);
		lista.add(taxi3);
      
    }
	

	


    // -----------------------------------------------------------------
    // M�todos de prueba
    // -----------------------------------------------------------------

    @Test
    public void testAdd( )
    {
    	Taxi taxi4 = new Taxi("Company4", "Taxi_id4");
    	
    	setupEscenario1( );
    	
    	 	
    	
    	try
        {
    		lista.add(taxi4);   
    	}
        catch( Exception e )
        {
        	 fail( "No se agrego correctamente" );
        }
        
       
        

       
        
        
       
    }

    @Test
    public void testDelete( )
    {
    	
    	
    	setupEscenario1( );

    	try
        {
    		lista.delete(taxi2);   
    	}
        catch( Exception e )
        {
        	fail();
        }
    		
    		
    }

    
    
    @Test
    public void testSize( )
    {setupEscenario1( );
    	 assertEquals( "El tama�o de la lista no es correcto", lista.size(), 3 );
    }
    
    @Test
    public void testGet2( )
    {
    	
    	
    	
    	setupEscenario1();
    	assertEquals( "No se obtuvo el elemento esperado", lista.get(0) , (taxi1) );
    	
    }
    
  
    @Test
    public void testGetCurrent( )
    {
    	   	
    	
    	setupEscenario1();
    	Taxi taxi4 = new Taxi("Company4", "Taxi_id4");
    	lista.add(taxi4); 
    	assertEquals( "No se obtuvo el elemento esperado", lista.getCurrent().equals(taxi4) );
    	

   
    }
    
    @Test
    public void testNext( )
    {
    	
    	setupEscenario1();
    	Taxi taxi4 = new Taxi("Company4", "Taxi_id4");
    	lista.add(taxi4);  
    	
    	assertTrue( "No se obtuvo el elemento esperado", lista.next().getCompany().equals(taxi4.getCompany()) ) ;
    	
    }
    
    @Test
    public void testBuscar( )
    {
    	setupEscenario1();
    	assertTrue( "Debia devolver true", lista.buscar(taxi2) );
    	
    }
    
    
	
	
}
