package test;

import static org.junit.Assert.*;

import org.junit.Test;

import model.data_structures.Lista;
import model.data_structures.Node;
import model.vo.Taxi;

public class NodoTest 
{
	String elemento1 = "Hola";
	String elemento2 = "Hello";
	String elemento3 = "Hi";
	
	Node<String> nodo1 = new Node<String>(elemento1);
	Node<String> nodo2 = new Node<String>(elemento2);
	Node<String> nodo3 = new Node<String>(elemento3);
	
	public void setupEscenario1( )
    {
		nodo2.cambiarAnterior(nodo1);
		nodo2.cambiarSiguiente(nodo3);
      
    }
	 // -----------------------------------------------------------------
    // M�todos de prueba
    // -----------------------------------------------------------------

    @Test
    public void testCambiarSiguiente( )
    {
    	nodo1.cambiarSiguiente(nodo2);
    	
    	
    	assertTrue("Se esperaba otra respuesta" , nodo1.darSiguiente().equals(nodo2));
    }
    
    @Test
    public void testCambiarAnterior( )
    {
    	nodo2.cambiarAnterior(nodo3);
    	
    	
    	assertTrue("Se esperaba otra respuesta" , nodo2.darAnterior().equals(nodo3));
    }
    
    @Test
    public void testDarSiguiente( )
    {
    	setupEscenario1( );
    	
    	assertTrue("Se esperaba otra respuesta" , nodo2.darSiguiente().darElemento().equals((nodo3).darElemento()));
    }
    
    @Test
    public void testDarAnterior( )
    {
    	setupEscenario1( );
    	
    	assertTrue("Se esperaba otra respuesta" , nodo2.darAnterior().darElemento().equals((nodo1).darElemento()));
    }
    
    @Test
    public void testDarElemento( )
    {
    	assertEquals("Se esperaba otra respuesta" , nodo2.darElemento() , "Hello");
    }
}
