package model.data_structures;

import model.vo.Service;

public class Lista <T extends Comparable<T>> implements LinkedList<T>
{

	private Node primero;
	
	private Node actual;
	
	private int contador;
	
	
	
	@Override
	public void add(T objeto) {
		// TODO Auto-generated method stub
		if( primero == null)
		{
			primero = new Node(objeto);
			contador++;
		}
		else
		{
			Node agregado = new Node(objeto);
			actual= primero;

			while(actual.darSiguiente()!=null)
			{
				actual = actual.darSiguiente();
			}

			actual.cambiarSiguiente(agregado);
			contador++;
		}
		
	}

	@Override
	public void delete(T objeto) {
		Node p = new Node(objeto);
		
				if(p.equals(primero))
				{
					primero = new Node(objeto);
					contador--;
				}
				else
				{
					p.darAnterior().cambiarSiguiente(p.darSiguiente());
					contador--;
				}
				
	}

	@Override
	public T get(T objeto) 
	{
		Node h = null;
		Node p = new Node(objeto);
		
		actual = primero;
		
		while(!(actual.equals(p)))
		{
			if(actual.equals(p))
			{
				h = actual;
			
			}
			else
			{
				actual = actual.darSiguiente();
			}
		}
			return (T) h.darElemento();
	}

	@Override
	public int size() {
		
		return contador;
	}

	@Override
	public T get(int pos) 
	{
	 T h = null;
		if(pos>=0 && pos<contador)
		{
			
			if(pos == 0 && primero != null)
			{
				h= (T) primero.darElemento();
			}
			
			else
			{
				actual = primero;
				for(int i=0; i<pos; i++)
				{
					actual = actual.darSiguiente();
				}
				h =(T) actual.darElemento();
			}
		}
		return h;
		
	}

	@Override
	public void listing() {
		
		actual = primero;
	}

	@Override
	public T getCurrent() {
		
		return (T) actual.darElemento();
	}

	@Override
	public T next() {
		// TODO Auto-generated method stub
		Node siguiente = null;
		if( actual.darSiguiente() != null)
		{
		siguiente = actual.darSiguiente();
		
		}
		
		return (T) siguiente.darElemento();
	}
	
	public boolean buscar(T servicio)
	{
		actual=primero;
		boolean encontrado = false;
		while(actual != null && encontrado != true)
		{
			if(servicio == actual.darElemento())
			{
				encontrado = true;
			}
			else
			{
				actual = actual.darSiguiente();
			}
		
		}
		return encontrado;
	}
	
	

}
