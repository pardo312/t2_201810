package model.data_structures;

public class Node <T extends Comparable<T>>
{
  private T elemento;
  
  private Node siguiente;
  
  private Node anterior;
  
  public Node( T info)
  {
	  elemento = info;
  }
  
  public void cambiarSiguiente(Node pSiguiente)
  {
	  siguiente = pSiguiente;
  }
  
  public void cambiarAnterior(Node pAnterior)
  {
	  anterior = pAnterior;
  }
  
  public Node darAnterior()
  {
	  return anterior;
  }
  
  public Node darSiguiente()
  {
	  return siguiente;
  }
  public T darElemento()
  {
	  return elemento;
  }
}
