package model.logic;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStreamReader;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;

import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import api.ITaxiTripsManager;
import model.vo.Taxi;
import model.vo.Service;
import model.data_structures.LinkedList;
import model.data_structures.Lista;

public class TaxiTripsManager implements ITaxiTripsManager {

	// TODO
	// Definition of data model 
	private Lista<Service> servicios = new Lista<Service>();
	private Lista<Taxi> taxis = new Lista<Taxi>();
	
	
	public void loadServices (String serviceFile) 
	{
		JsonParser parser = new JsonParser();
		try 
		{ 
			File f = new File("./data/taxi-trips-wrvz-psew-subset-small.json");
			JsonArray arr= (JsonArray) parser.parse(new FileReader(f)); 
		
	       
    		Gson gson = new GsonBuilder().create();
    		
	        // Read file in stream mode
	       
	        for (int i = 0; arr != null && i < arr.size(); i++)
			{
	        	
				JsonObject obj= (JsonObject)arr.get(i);
				
				String company = "NaN";
				
				if ( obj.get("company") != null )
						{ 
							company = obj.get("company").getAsString();
							
						}
				
				String dropoff_census_tract = "NaN";
				
				if ( obj.get("dropoff_census_tract") != null )
				{ 
					dropoff_census_tract = obj.get("dropoff_census_tract").getAsString();
					
				}
				
				String dropoff_centroid_latitude = "NaN";
				
				if ( obj.get("dropoff_centroid_latitude") != null )
				{ 
					dropoff_centroid_latitude= obj.get("dropoff_centroid_latitude").getAsString();
					
				}
				
				String dropoff_community_area = "NaN";
				
				if ( obj.get("dropoff_community_area") != null )
				{ 
					dropoff_community_area = obj.get("dropoff_community_area").getAsString();
					
				}
				
				//con 2 datos
				JsonObject dropoff_localization_obj = null;
				double longitudeDrop = 0.0;
				double latitudeDrop = 0.0;
				if ( obj.get("dropoff_centroid_location") != null )
				{ 
					dropoff_localization_obj =obj.get("dropoff_centroid_location").getAsJsonObject();
					
				
				  JsonArray dropoff_localization_arr = ( dropoff_localization_obj).get("coordinates").getAsJsonArray();
				  
				 longitudeDrop = dropoff_localization_arr.get(0).getAsDouble();
				 latitudeDrop = dropoff_localization_arr.get(1).getAsDouble();
				 
				}
				else 
				{
					
				}
				
				String dropoff_centroid_longitude  = "NaN";
				
				if ( obj.get("dropoff_centroid_longitude") != null )
				{ 
					dropoff_centroid_longitude = obj.get("dropoff_centroid_longitude").getAsString();
					
				}
				
				
				String extras = "NaN";
				
				if ( obj.get("extras") != null )
				{ 
					extras = obj.get("extras").getAsString();
					
				}
				
				String fare = "NaN";
				
				if ( obj.get("fare") != null )
				{ 
					fare = obj.get("fare").getAsString();
					
				}
				
				String payment_type = "NaN";
				
				if ( obj.get("payment_type") != null )
				{ 
					payment_type = obj.get("payment_type").getAsString();
					
				}
				
				String pickup_census_tract = "NaN";
				
				if ( obj.get("pickup_census_tract") != null )
				{ 
					pickup_census_tract = obj.get("pickup_census_tract").getAsString();
					
				}
				
				String pickup_centroid_latitude = "NaN";
				
				if ( obj.get("pickup_centroid_latitude") != null )
				{ 
					pickup_centroid_latitude = obj.get("pickup_centroid_latitude").getAsString();
					
				}
				
				JsonObject pickup_localization_obj = null;
				double longitudePickup = 0.0;
				double latitudePickup = 0.0;
				
				if ( obj.get("pickup_centroid_location") != null )
				{ 
					
					
					pickup_localization_obj =obj.get("pickup_centroid_location").getAsJsonObject();
					
					
					  JsonArray pickup_localization_arr = ( pickup_localization_obj).get("coordinates").getAsJsonArray();
					  
					  longitudePickup = pickup_localization_arr.get(0).getAsDouble();
					  latitudePickup = pickup_localization_arr.get(1).getAsDouble();
					 
					
				}
				
				String pickup_centroid_longitude = "NaN";
				
				if ( obj.get("pickup_centroid_longitude") != null )
				{ 
					pickup_centroid_longitude = obj.get("pickup_centroid_longitude").getAsString();
					
				}
						
				
				String pickup_community_area = "NaN";
				
				if ( obj.get("pickup_community_area") != null )
				{ 
					pickup_community_area	 = obj.get("pickup_community_area").getAsString();
					
				}
				
				
				String taxi_id = "NaN";
				
				if ( obj.get("taxi_id") != null )
				{ 
					taxi_id = obj.get("taxi_id").getAsString();
					
				}
				
				
				String tips = "NaN";
				
				if ( obj.get("tips") != null )
				{ 
					tips = obj.get("tips").getAsString();
					
				}
				
				
				String tolls  = "NaN";
				
				if ( obj.get("tolls") != null )
				{ 
					tolls = obj.get("tolls").getAsString();
					
				}
				
				
				String trip_end_timestamp = "NaN";
				
				if ( obj.get("trip_end_timestamp") != null )
				{ 
					trip_end_timestamp = obj.get("trip_end_timestamp").getAsString();
					
				}
				
				
				String trip_id = "NaN";
				
				if ( obj.get("trip_id") != null )
				{ 
					trip_id	 = obj.get("trip_id").getAsString();
					
				}
				
				String trip_miles = "NaN";
				
				if ( obj.get("trip_miles") != null )
				{ 
					trip_miles = obj.get("trip_miles").getAsString();
					
				}
				
				String trip_seconds = "NaN";
				
				if ( obj.get("trip_seconds") != null )
				{ 
					trip_seconds = obj.get("trip_seconds").getAsString();
					
				}
				
				String trip_start_timestamp = "NaN";
				
				if ( obj.get("trip_start_timestamp") != null )
				{ 
					trip_start_timestamp = obj.get("trip_start_timestamp").getAsString();
					
				}
				
				String  trip_total= "NaN";
				
				if ( obj.get("trip_total") != null )
				{ 
					trip_total = obj.get("trip_total").getAsString();
					
				}

				
		
			
					

		      
				
				Service servicioI = new Service(company,dropoff_census_tract , dropoff_centroid_latitude,dropoff_community_area, longitudeDrop, latitudeDrop, dropoff_centroid_longitude, dropoff_community_area , extras, fare, payment_type, pickup_census_tract,pickup_centroid_latitude,longitudePickup,latitudePickup, pickup_centroid_longitude,pickup_community_area, taxi_id, tips, tolls, trip_end_timestamp, trip_id, trip_miles, trip_seconds, trip_start_timestamp, trip_total);	
				servicios.add(servicioI);
				
				Taxi taxisI = new Taxi(taxi_id,company);
				taxis.add(taxisI);
				
				  
			}

			 System.out.println( "Datos Cargados!!");
			 
		}
		catch (JsonIOException e1 ) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		catch (JsonSyntaxException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		catch (FileNotFoundException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
		
	}

	@Override
	public Lista<Taxi> getTaxisOfCompany(String company) 
	{
		Lista<Taxi> taxisCompania = new Lista<Taxi>();
		
		for(int i = 0; taxis != null && i < taxis.size(); i++)
		{
			if(taxis.get(i).getCompany().equals(company))
			{
				
					if(taxisCompania.buscar(taxis.get(i))== false)
							{
								taxisCompania.add(taxis.get(i));
							}
				
			}
		}
		
	
		return taxisCompania;
		
	}

	@Override
	public Lista<Service> getTaxiServicesToCommunityArea(int communityArea) 
	
	{
		Lista<Service> service = new Lista<Service>();
		
				for(int i = 0; i < servicios.size(); i++)
						{	
							Service p =servicios.get(i);
							int h = p.getArea();
							if( h == (communityArea))
									{
										
													service.add(servicios.get(i));	
									}
						}
	
		return service;
}


}
