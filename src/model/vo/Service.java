package model.vo;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {

	private String company;

	private String dropoff_census_tract;

	private String dropoff_centroid_latitude;
	private String dropoff_comunity_area;
	
	private Double longitudeDrop;
	private Double latitudeDrop;
	
	
	private String extras;
	private String fare;
	
	private String payment;
	private String pickup_census_tract;
	private String pickup_centroid_latitude;
	
	private Double longitudePickup;
	private Double latitudePickup;
	
	private String pickup_centroid_longitude;
	private String pickup_community_area;
	private String taxi_id;
	private String tips;
	private String tolls;
	private String trip_end_timestamp;
	private String trip_id;
	private String trip_miles;
	private String trip_seconds;
	private String trip_start_timestamp;
	private String trip_total;
	
	public Service(String qcompany, String pdropoff_census_tract, String pdropoff_centroid_latitude,String pdropoff_comunity_area,Double plongitudeDrop, Double platitudeDrop,String pdropoff_centroid_longitude,String pdropoff_community_area,String pextras, String pfare, String ppayment,String ppickup_census_tract, String ppickup_centroid_latitude,Double plongitudePickup, Double platitudePickup,String ppickup_centroid_longitude, String ppickup_community_area, String ptaxi_id, String ptips ,String ptolls, String ptrip_end_timestamp, String 	ptrip_id, String ptrip_miles, String ptrip_seconds, String ptrip_start_timestamp, String ptrip_total)
	{
		company = qcompany;
		dropoff_census_tract=pdropoff_census_tract ;
		dropoff_centroid_latitude =pdropoff_centroid_latitude;
		dropoff_comunity_area = pdropoff_comunity_area;
		
		longitudeDrop = plongitudeDrop;
		latitudeDrop=platitudeDrop;
		
		
		
		extras = pextras;
		fare = pfare;
		payment =ppayment;
		pickup_census_tract = ppickup_census_tract;
		pickup_centroid_latitude = ppickup_centroid_latitude;
		
		longitudePickup = plongitudePickup;
		latitudePickup=platitudePickup;
		
		pickup_centroid_longitude = ppickup_centroid_longitude;
		pickup_community_area = ppickup_community_area;
		taxi_id = ptaxi_id;
		tips = ptips;
		tolls = ptolls;
		trip_end_timestamp = ptrip_end_timestamp;
		trip_id = ptrip_id;
		
		trip_miles = ptrip_miles;
		trip_seconds = ptrip_seconds;
		trip_start_timestamp = ptrip_start_timestamp;
		trip_total= ptrip_total;
				
	}
	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		// TODO Auto-generated method stub
		return trip_id;
	}	

	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxi_id;
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		int p = Integer.parseInt(trip_seconds);
		return p;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public String getTripMiles() {
		// TODO Auto-generated method stub
		
		return trip_miles;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		double p = Double.parseDouble(trip_total);
		return p;
	}
	
	public int getArea()
	{
		int h = 0;
		if(dropoff_comunity_area != "NaN" )
			{
			int i = Integer.parseInt(dropoff_comunity_area);
			h = i  ;
			}
		
		return h;
	}
	


	@Override
	public int compareTo(Service o) {
		// TODO Auto-generated method stub
		return 0;
	}
}
