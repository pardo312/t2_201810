package model.vo;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>{

	private String taxi_id;
	
	private String company;
	public Taxi (String ptaxi_id, String pcompany)
	{
		taxi_id = ptaxi_id;
		company = pcompany;
	}
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxi_id;
	}

	/**
	 * @return company
	 */
	public String getCompany() {
		// TODO Auto-generated method stub
		return company;
	}
	
	@Override
	public int compareTo(Taxi o) {
		// TODO Auto-generated method stub
		
		int h ;
		if(this.hashCode() == o.hashCode())
		{
			h = 0;
		}
		else
		{
			h = -1;
		}
		return h;
	}	
}
